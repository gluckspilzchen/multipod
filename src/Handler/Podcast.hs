{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Podcast where

import Import
import Text.Julius

getPodcastR :: String -> Handler Html
getPodcastR podName =
    defaultLayout $ do
        setTitle (toHtml podName)
        let name = rawJS podName
        $(widgetFile "podcast")

getEpisodesR :: String -> Handler Value
getEpisodesR name = do
    pod <- runDB $ getPodcastFromName name
    case pod of
        Just (Entity idPod _) -> do
            episodes <- runDB $ getEpisodesFromPodcastId idPod
            returnJson $ map episodeData episodes
        Nothing -> returnJson ([] :: [Value])
  where
    episodeData (Entity key episode) =
        let idEp = show key
            nameEp = episodeName episode
            url = episodeUrl episode
            isRead = episodeIsRead episode
            isInPlaylist = isJust $ episodePlaylist episode
            currentTime = episodeCurrentTime episode
        in object $
           ["id" .= idEp, "url" .= url, "title" .= nameEp, "isRead" .= isRead, "isInPlaylist" .= isInPlaylist, "currentTime" .= currentTime]
