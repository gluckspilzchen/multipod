module Import.Crypto
    ( Import.Crypto.encrypt
    , Import.Crypto.decrypt
    ) where

import Crypto.Cipher.ChaChaPoly1305 as C
import Crypto.Error
import Data.ByteArray
import Data.ByteString.Char8 as BS

crypt ::
       (ByteString -> State -> (ByteString, State))
    -> ByteString -- nonce (12 random bytes)
    -> ByteString -- symmetric key
    -> ByteString -- input
    -> Maybe (ByteString, ByteString) -- ciphertext with a 128-bit tag attached
crypt f nonce key plaintext = case aux of
        CryptoPassed v -> Just v
        CryptoFailed _ -> Nothing
    where
        aux = do
            st1 <- C.nonce12 nonce >>= C.initialize key
            let (out, st3) = f plaintext st1
                auth = C.finalize st3
            return $ (out, Data.ByteArray.convert auth)

encrypt ::
       ByteString -- nonce (12 random bytes)
    -> ByteString -- symmetric key
    -> ByteString -- input
    -> Maybe (ByteString, ByteString)
encrypt = crypt C.encrypt

decrypt ::
       ByteString -- nonce (12 random bytes)
    -> ByteString -- symmetric key
    -> ByteString -- input
    -> Maybe (ByteString, ByteString)
decrypt = crypt C.decrypt
