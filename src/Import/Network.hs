module Import.Network
    ( getBody
    ) where

import Control.Monad.IO.Class
import Data.ByteString.Lazy
import Data.String
import Network.HTTP.Simple

getBasicBody :: (MonadIO m) => String -> m ByteString
getBasicBody request = do
    response <- httpLBS $ fromString request
    return $ getResponseBody response

getAuthorizedBody :: (MonadIO m) => (String, String) -> String -> m ByteString
getAuthorizedBody (login, password) request = do
    response <-
        httpLBS $
        setRequestBasicAuth (fromString login) (fromString password) $
        fromString request
    return $ getResponseBody response

getBody :: (MonadIO m) => Maybe (String, String) -> String -> m ByteString
getBody Nothing = getBasicBody
getBody (Just creds) = getAuthorizedBody creds
