{-# LANGUAGE OverloadedStrings #-}

module Import.PodcastReader
    ( getEpisodeTitle
    , getEpisodeUrl
    , getPodcastTitle
    , getPodcastLogo
    , getPodcastEpisodes
    , getEpisodeGuid
    ) where

import Data.Text as T
import Text.XML.Cursor

getPodcastEpisodes :: Cursor -> (Cursor -> [T.Text]) -> [T.Text]
getPodcastEpisodes c f = c $/ element "channel" &/ element "item" &/ f

getPodcastTitle :: Cursor -> T.Text
getPodcastTitle c =
    T.concat $ c $/ element "channel" &/ element "title" &// content

getPodcastLogo :: Cursor -> T.Text
getPodcastLogo c =
    T.concat $
    c $/ element "channel" &/
    element "{http://www.itunes.com/dtds/podcast-1.0.dtd}image" >=>
    attribute "href"

getEpisodeTitle :: Cursor -> [T.Text]
getEpisodeTitle = element "title" &// content

getEpisodeUrl :: Cursor -> [T.Text]
getEpisodeUrl = element "enclosure" >=> attribute "url"

getEpisodeGuid :: Cursor -> [T.Text]
getEpisodeGuid = element "guid" &// content
